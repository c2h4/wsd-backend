# wsd-backend

> 微闪贷后台管理系统

## About

> VUE2+IView 
>
> vuex
>
> vue-router
>
> axios
>
> less
>
> webpack
>
> babel + eslint

## Build Setup

``` bash
# install dependencies
npm install

# 国内
npm install --registry=https://registry.npm.taobao.org

# serve with hot reload at localhost:9999
npm run dev

# build for production with minification
npm run build

```

## 项目结构

```bash
├─build                 // 打包环境
│      
├─config                // 开发部署配置
│      
├─node_modules
│  
├─src                   // 项目源文件
│  │  main.js           // 入口文件
│  ├─api                // 请求接口
│  │      
│  ├─assets             // 组件静态资源
│  │  └─styles          // 样式
│  │      ├─base        // 基础样式
│  │      ├─cover       // iview覆盖样式
│  │      ├─layout      // 布局样式
│  │      └─variable    // 样式变量
│  │                                          
│  │  
│  ├─components     
│  │  │  
│  │  ├─layout          // 布局组件
│  │  │      Theader.vue
│  │  │      Nav.vue
│  │  ├─pages           // 基本组件
│  │  │      E404.vue
│  │  │      Login.vue
│  │  │      
│  │  └─views           // 业务组件
│  │          
│  ├─router             // 路由管理
│  │      
│  └─store              // 状态管理
│              
├─static                // 业务静态资源
│  └─img
│          
|
│  .babelrc             // babel配置
│  .editorconfig        // editor配置
│  .gitignore           // git忽略配置
│  .eslintrc.js         // eslint配置
│  .postcssrc.js        // postcss配置
│  index.html           // 项目首页
│  package.json         // 依赖配置
│  prod.server.js       // 打包后启动文件
```

> 代码默认遵循eslint(default)规范。
>
> 可以根目录下的eslintrc.js修改规则，或在 /build/webpack.base.conf.js 关闭相应规则

## 说明

#### 路由设计

- 登录路由拦截：`requiresAuth` 字段判断某路由的访问是否需要登录

- 

- 登录成功后首先显示主页 `index` 为管理员个人信息，此页面也可以通过 `header` 右方的个人信息进入查看；

- 用户登录后将登录信息存储在 `token` 中，退出登录后则清除 `token`，重定向到登录界面；


#### 数据增删查接口

#### 页面功能说明

- `header`
  - 收起侧边导航栏
  - 语言切换
  - 通知提醒（目前仅图标）
  - 用户下拉栏
    - 个人信息：跳转至个人信息
    - 退出登录：移除 `cookies` 并跳转登录界面
    - 锁定屏幕
  - 切换全屏
- `sidebar`
  - 默认 `submenu` 收起，点击展开图标进行二级菜单选择

