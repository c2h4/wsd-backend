// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')

var config = {
  build: {
    env: require('./prod.env'),
    port: 9000,
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    baseServerUrl: '/',
    productionSourceMap: true,
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    port: 8388,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    baseServerUrl: 'http://49.234.90.210:8388',
    proxyTable: {},
    cssSourceMap: false
  }
}

// 需要代理的接口
var proxyList = ['/manage']

const targetPath = config.dev.baseServerUrl // 服务器的地址
for (let i = 0; i < proxyList.length; i++) {
  config.dev.proxyTable[proxyList[i]] = {
    target: targetPath,
    secure: false,
    changeOrigin: true
  }
  console.log(config.dev.proxyTable[proxyList[i]])
}
// console.info(Object.keys(config.dev.proxyTable))
module.exports = config
