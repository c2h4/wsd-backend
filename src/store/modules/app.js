// import router from '@/router'
import iView from 'iview'
import Vue from 'vue'
Vue.use(iView)

const app = {
  state: {
    sidebar: {
      opened: true,
      minOpen: false // 小屏时菜单状态
    },
    router: {
      currentPageName: 'index'
    },
    langList: [
      {
        value: 'CN',
        label: '中文简体'
      },
      {
        value: 'TW',
        label: '中文繁体'
      },
      {
        value: 'EN',
        label: 'English'
      }
    ],
    lang: 'CN', // 默认中文简体
    lock: '0', // 默认不锁
    lockPage: '/index' // 锁屏前的页面
  },
  mutations: {
    /**
     * 菜单的缩展
     */
    CLOSE_SLIDEBAR: state => {
      state.sidebar.opened = false
      state.sidebar.minOpen = false
    },
    OPEN_SLIDEBAR: state => {
      state.sidebar.opened = true
    },
    /**
     * 面包屑
     */
    GET_CURRENT_PAGE_NAME: (state, payload) => {
      state.router.currentPageName = payload
    }
  },
  actions: {
    ToggleSideBar: ({commit}) => {
      commit('TOGGLE_SIDEBAR')
    },
    getCurrentPageName: ({commit}, payload) => {
      commit('GET_CURRENT_PAGE_NAME', payload)
    }
  }
}

export default app
