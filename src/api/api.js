import fetch from './fetch.js'

/**
 * 登录
 * @param params
 */
const login = params => {
  return fetch({
    url: '/quickLoan/doLogin',
    method: 'post',
    data: JSON.stringify(params)
  })
}

/**
 * 提交用户注册信息
 * @param params

const postUserInfo = params => {
  return fetch({
    url: ``,
    method: 'post',
    data: params
  })
}
*/

/**
 * 获取用户信息
 * @param params String state 0正常 1 短期逾期 2 长期逾期
 *               int pageNum  页号
 *               int pageSize 页面显示条数
 */
const getUserList = params => {
  return fetch({
    url: `/manage/getUserListByState?state=${params.state}&pageNum=${params.pageNum}&pageSize=${params.pageSize}`,
    method: 'get',
    params: ''
  })
}

/**
 * 获取贷款报表
 * @param params
 */
const getLoanList = params => {
  return fetch({
    url: `/manage/getLoanList`,
    method: 'get',
    params: params
  })
}

/**
 * 查看指定用户信息
 * @param params
 */
const getUserById = params => {
  return fetch({
    url: `/manage/getUserById?userId=${params.customer_id}`,
    method: 'get',
    params: ''
  })
}

/**
 * 获取正常放款统计
 * @param params
 */
const getNormalLoanCount = params => {
  return fetch({
    url: '/manage/getNormalLoanCount',
    method: 'get',
    params: params
  })
}

/**
 * 获取正常放款统计详情表
 * @param params
 */
const getNormalLoanCountList = params => {
  return fetch({
    url: `/manage/getNormalLoanCountList`,
    method: 'get',
    params: params
  })
}

/**
 * 获取逾期统计
 * @param params type 0 短期 1 长期
 */
const getLoanCount = params => {
  return fetch({
    url: `/manage/getLoanCount?type=${params.type}`,
    method: 'get',
    params: ''
  })
}

/**
 * 获取逾期统计详情表
 * @param params type 0 短期 1 长期
 */
const getLoanCountList = params => {
  return fetch({
    url: `/manage/getLoanCountList?type=${params.type}&pageNum=${params.pageNum}&pageSize=${params.pageSize}`,
    method: 'get',
    params: ''
  })
}

/**
 * 新增管理员信息
 * @param params
 */
const setAdmin = params => {
  return fetch({
    url: `/quickLoan/setAdmin`,
    method: 'get',
    params: params
  })
}

/**
 * 删除管理员信息
 * @param params
 */
const removeAdmin = params => {
  return fetch({
    url: `/quickLoan/removeAdmin`,
    method: 'get',
    params: params
  })
}

/**
 * 获取所有管理员信息
 * @param params
 */
const getAdmin = params => {
  return fetch({
    url: '/quickLoan/getAdminsInfo',
    method: 'get',
    params: params
  })
}

/**
 * 验证邮箱 token是否过期
 * @param params
 */
const checkToken = params => {
  return fetch({
    url: '',
    method: 'get',
    params: params
  })
}

const apiList = {
  login,
  getUserList,
  getLoanList,
  getUserById,
  getNormalLoanCount,
  getNormalLoanCountList,
  getLoanCount,
  getLoanCountList,
  setAdmin,
  removeAdmin,
  getAdmin,
  checkToken
}

export default apiList
