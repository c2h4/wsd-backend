const menu = [
  {
    name: '客户信息',
    icon: 'icon-lvshihan',
    level: 0,
    fixed: false,
    children: [
      {
        path: '/info-normal',
        name: '正常状态',
        icon: 'icon-enterinto',
        level: 1,
        children: [],
        fixed: false
      },
      {
        path: '/info-short',
        name: '短期逾期',
        icon: 'icon-enterinto',
        level: 1,
        children: [],
        fixed: false
      },
      {
        path: '/info-long',
        name: '长期逾期',
        icon: 'icon-enterinto',
        level: 1,
        children: [],
        fixed: false
      }
    ]
  },
  {
    path: '/credit', // 点击贷款报表默认进入已进行贷款报表
    name: '贷款报表',
    icon: 'icon-activity',
    level: 1,
    fixed: false,
    children: []
  },
  {
    path: '/loan',
    name: '放款总额',
    level: 1,
    icon: 'icon-order',
    fixed: false,
    children: []
  },
  {
    path: '/',
    name: '逾期总额',
    level: 0,
    icon: 'icon-document',
    fixed: false,
    children: [
      {
        path: '/overdueshort',
        name: '短期逾期',
        icon: 'icon-dynamic_fill',
        level: 1,
        children: [],
        fixed: false
      },
      {
        path: '/overduelong',
        name: '长期逾期',
        level: 1,
        children: [],
        fixed: false
      }
    ]
  },
  {
    path: '/manage',
    name: '管理权限',
    icon: 'icon-dynamic_fill',
    level: 1,
    children: [],
    fixed: false
  }
]

export default menu
