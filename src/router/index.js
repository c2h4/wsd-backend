import Vue from 'vue'
import store from '@/store'
import Router from 'vue-router'
import iView from 'iview'
import routes from './routes'
// import Cookies from 'js-cookie'
Vue.use(iView)
Vue.use(Router)

const router = new Router({
  routes
})

router.beforeEach((to, from, next) => {
  if (document.documentElement.clientWidth <= 600) {
    store.commit('CLOSE_SLIDEBAR')
  }
  if (to.meta.requiresAuth) {
    let token = sessionStorage.authotization
    if (token != null && token !== undefined) {
      store.dispatch('getCurrentPageName', to.name)
      next()
    } else {
      console.log('not login')
      next({
        path: '/login',
        query: {redirect: '/index'}
      })
    }
  } else {
    store.dispatch('getCurrentPageName', to.name)
    next()
  }
  /*
  if (to.meta.permission) { // 此处判断是否有超级管理员权限
    if (to.path === '/manage') {
      next({path: '/nopermission'}) // 没有超级管理员权限
    }
  }
  */
})

/*

*/

export default router
