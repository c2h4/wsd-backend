import Home from '@/components/layout/Home'
import Index from '@/components/views/Index'

const NotFound = () => import('@/components/pages/NotFound')
const E404 = () => import('@/components/pages/E404')
const Lock = () => import('@/components/layout/base/Lock')

const Login = () => import('@/components/pages/Login')
const Info = () => import('@/components/views/Info')
const Credit = () => import('@/components/views/Credit')
const Loan = () => import('@/components/views/Loan')
// const OverdueShort = () => import('@/components/views/OverdueShort')
const Overdue = () => import('@/components/views/Overdue')
const Manage = () => import('@/components/views/Manage')
const NoPermission = () => import('@/components/pages/NoPermission')
const Test = () => import('@/components/views/test')

const routes = [
  {
    path: '/',
    name: 'wsd-backend',
    component: Home,
    redirect: '/login',
    children: [
      {
        path: '/info-normal',
        name: '正常状态',
        component: Info,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          state: '0',
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/info-short',
        name: '短期逾期',
        component: Info,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          state: '1',
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/info-long',
        name: '长期逾期',
        component: Info,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          state: '2',
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/credit',
        name: '',
        component: Credit,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/credit-ing',
        name: '',
        component: Credit,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/credit-end',
        name: '',
        component: Credit,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/loan',
        name: '',
        component: Loan,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/overdueshort',
        name: '短期逾期',
        component: Overdue,
        icon: 'icon-mine',
        level: 1,
        meta: {
          type: '0',
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/overduelong',
        name: '长期逾期',
        component: Overdue,
        icon: 'icon-mine',
        level: 1,
        meta: {
          type: '1',
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/manage',
        name: '',
        component: Manage,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/index',
        name: '个人信息',
        component: Index,
        icon: 'icon-mine',
        level: 1,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/notFound',
        name: 'notFound',
        component: NotFound,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      },
      {
        path: '/nopermission',
        name: '访问限制',
        component: NoPermission,
        meta: {
          requiresAuth: true, // 是否需要登录
          permission: false // 是否需要超级管理员权限
        }
      }
    ]
  },
  {
    path: '/test',
    name: '',
    component: Test,
    icon: 'icon-mine',
    level: 1
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/error404',
    name: 'error404',
    component: E404
  },
  {
    path: '/lock',
    name: 'lock',
    component: Lock
  },
  {
    path: '*',
    redirect: '/notFound'
  }
]

export default routes
